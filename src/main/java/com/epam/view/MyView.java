package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", " 1 - print house list");
        menu.put("2", " 2 - sort house list by price");
        menu.put("3", " 3 - sort house list by distance to kindergarten");
        menu.put("4", " 4 - sort house list by distance to school");
        menu.put("5", " 5 - sort house list by distance to playground");
        menu.put("Q", " Q - Exit");

        this.methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println(controller.getHouseList());
    }

    private void pressButton2() {
        System.out.println("Please input maximum price:");
        int amount = input.nextInt();
        System.out.println(controller.sortHouseListByPrice(amount));
    }

    private void pressButton3() {
        System.out.println("Please input maximum distance to kindergarten:");
        int amount = input.nextInt();
        System.out.println(controller.sortHouseListByDistanceToKindergarten(amount));
    }

    private void pressButton4() {
        System.out.println("Please input maximum distance to school:");
        int amount = input.nextInt();
        System.out.println(controller.sortHouseListByDistanceToSchool(amount));
    }

    private void pressButton5() {
        System.out.println("Please input maximum distance to playground:");
        int amount = input.nextInt();
        System.out.println(controller.sortHouseListByDistanceToPlayground(amount));
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
