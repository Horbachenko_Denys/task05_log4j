package com.epam.model.Comparator;

import com.epam.model.House;

import java.io.Serializable;
import java.util.Comparator;

public class DistanceToKindergartenComparator implements Comparator<House>, Serializable {
    @Override
    public int compare(House h1, House h2) {
        return Integer.compare(h1.getDisToKindergarten(), h2.getDisToKindergarten());
    }
}
