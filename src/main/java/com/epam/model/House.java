package com.epam.model;

/**
 * Class create List of houses.
 */

import com.epam.model.Comparator.DistanceToKindergartenComparator;
import com.epam.model.Comparator.DistanceToPlaygroundComparator;
import com.epam.model.Comparator.DistanceToSchoolComparator;
import com.epam.model.Comparator.PriceComparator;
import com.epam.model.HouseType.Apartment;
import com.epam.model.HouseType.Mansion;
import com.epam.model.HouseType.Penthouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class House {

    public static Logger logger = LogManager.getLogger(House.class);
    private static List<House> houseList;
    public List<House> sortedList;
    private int priceOfRent;
    private int disToKindergarten;
    private int disToSchool;
    private int disToPlayground;

    public House() {
        if (houseList == null) {
            houseList = new LinkedList<>();
            generateHousingList();
        }
    }

    public int getPriceOfRent() {
        return priceOfRent;
    }

    public void setPriceOfRent(int priceOfRent) {
        this.priceOfRent = priceOfRent;
    }

    public int getDisToKindergarten() {
        return disToKindergarten;
    }

    public void setDisToKindergarten(int disToKindergarten) {
        this.disToKindergarten = disToKindergarten;
    }

    public int getDisToSchool() {
        return disToSchool;
    }

    public void setDisToSchool(int disToSchool) {
        this.disToSchool = disToSchool;
    }

    public int getDisToPlayground() {
        return disToPlayground;
    }

    public void setDisToPlayground(int disToPlayground) {
        this.disToPlayground = disToPlayground;
    }

    private void generateHousingList() {
        Random rnd = new Random();
        for (int i = 0; i < 10; i++) {
            switch (rnd.nextInt(3)) {
                case 0:
                    houseList.add(new Apartment());
                    break;
                case 1:
                    houseList.add(new Mansion());
                    break;
                case 2:
                    houseList.add(new Penthouse());
                    break;
                default:
                    System.out.println("Error in HouseList creation");
                    break;
            }
        }
        logger.info("This is an info message");
    }

    public List<House> getHouseList() {
        logger.debug("This is a debug massage");
        return houseList;
    }

    public List<House> sortHouseListByPrice(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getPriceOfRent() <= amount) {
                sortedList.add(h);
            }
            PriceComparator myPriceComparator = new PriceComparator();
            sortedList.sort(myPriceComparator);
        }
        logger.trace("This is a trace massage");
        return sortedList;
    }

    public List<House> sortHouseListByDistanceToKindergarten(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToKindergarten() <= amount) {
                sortedList.add(h);
            }
            DistanceToKindergartenComparator myKindergartenComp = new DistanceToKindergartenComparator();
            sortedList.sort(myKindergartenComp);
        }
        logger.warn("This is a warn massage");
        return sortedList;
    }

    public List<House> sortHouseListByDistanceToSchool(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToSchool() <= amount) {
                sortedList.add(h);
            }
            DistanceToSchoolComparator mySchoolComp = new DistanceToSchoolComparator();
            sortedList.sort(mySchoolComp);
        }
        logger.error("This is an error massage");
        return sortedList;
    }

    public List<House> sortHouseListByDistanceToPlayground(int amount) {
        sortedList = new LinkedList<>();
        for (House h : houseList) {
            if (h.getDisToPlayground() <= amount) {
                sortedList.add(h);
            }
            DistanceToPlaygroundComparator myPlaygroundComp = new DistanceToPlaygroundComparator();
            sortedList.sort(myPlaygroundComp);
        }
        logger.fatal("This is a fatal massage");
        return sortedList;
    }
}
