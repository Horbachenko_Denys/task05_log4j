package com.epam.model;

import java.util.List;

public interface Model {

    List<House> getHouseList();

    List<House> sortHouseListByPrice(int amount);

    List<House> sortHouseListByDistanceToKindergarten(int amount);

    List<House> sortHouseListByDistanceToSchool(int amount);

    List<House> sortHouseListByDistanceToPlayground(int amount);
}
