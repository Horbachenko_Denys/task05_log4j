package com.epam.model;

import java.util.List;

public class BusinessLogic implements Model {

    private House house;


    public BusinessLogic() {
        house = new House();
    }

    @Override
    public List<House> getHouseList() {
        return house.getHouseList();
    }

    @Override
    public List<House> sortHouseListByPrice(int amount) {
        return house.sortHouseListByPrice(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToKindergarten(int amount) {
        return house.sortHouseListByDistanceToKindergarten(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToSchool(int amount) {
        return house.sortHouseListByDistanceToSchool(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToPlayground(int amount) {
        return house.sortHouseListByDistanceToPlayground(amount);
    }
}
